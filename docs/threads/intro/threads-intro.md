[TOC]

# Creando un thread básico

La función `pthread_create()` crea un nuevo _thread_ desde el proceso que lo llama en el `main`, el primer parámetro es un puntero a una variable de tipo `pthread_t` declarada como `pthread_t thread_id;` donde se guarda el ID del _thread_ creado para usar en el resto del programa.

Creando el _thread_ desde la aplicación principal `main()`:

```c
    pthread_create(&thread_id, NULL, myThreadFun, NULL);
```

`pthread_create()` retorna 0 si tuvo éxito o -1 si se produjo error (consultar `errno`).

La función `myThreadFun` se ejecuta en el nuevo _thread_, la función debe devolver un puntero nulo `void *` y recibirlo también como argumento (un puntero a `void` puede apuntar a cualquier tipo de dato, un buen ejemplo es la función `malloc` que retorna un puntero nulo). `myThreadFun` se pasa como tercer parámetro a `pthread_create()` con la siguiente sintaxis:

```c
void *(*start_routine) (void *)
```

Definición de la función `myThreadFun`:

```c
void *myThreadFun()
{
    sleep(1);
    printf("Soy un thread.\n");
    return NULL;
}
```

# Esperando la finalización de un thread

La función `pthread_join()` recibe como parámetro el ID del thread que acabamos de crear y espera a la finalización del mismo.

```c
pthread_join(thread_id, NULL);
```

# Enlaces externos

- ["pthread_create"](http://man7.org/linux/man-pages/man3/pthread_create.3.html).
- ["pthread_join"](http://man7.org/linux/man-pages/man3/pthread_join.3.html): Espera que un _thread_ termine.
