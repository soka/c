#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

void *myThreadFun()
{
    sleep(1);
    printf("Soy un thread.\n");
    return NULL;
}

int main()
{
    pthread_t thread_id;
    printf("Antes del thread\n");
    if (pthread_create(&thread_id, NULL, myThreadFun, NULL) != 0)
    {
        perror("pthread_create() error");
    }
    pthread_join(thread_id, NULL);
    printf("Después del  thread\n");
    exit(0);
}