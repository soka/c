#include <stdio.h>

void main()
{
    int *ptr = NULL;
    printf("%d.\n", NULL);

    int num = 10;
    ptr = &num;
    printf("Num is:%d.\n", (*ptr));

    printf("Ptr Addr is: %p.\n", ptr);

    int number = 15;
    int *pointer = &number;
    int result = 0;
    result = *pointer + 5;

    printf("number address: %p, pointer address:%p.\n", &number, pointer);

    pointer = NULL;
    printf("Pointer size in bytes: %d.\n", sizeof(pointer));
}