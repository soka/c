[TOC]

# Como funciona un puntero

![](img/01.PNG)

# Declaración de un puntero

Es una nueva práctica inicializar el puntero a `NULL` en el mismo momento que se declara.

```c
int* ptr = NULL;
```

`NULL` es una constante definida en la librería estándar (en `<stdlib.h>`, `<stdio.h` y otras más).

Si quisiera que mi puntero apunte a la misma dirección que una variable declarada previamente:

```c
    int num = 10;
    ptr = &num;
```

Puedo imprimir la dirección del puntero si quiero:

```c
printf("Ptr Addr is: %p.\n", ptr);
```

Genera una salida como esta:

```ps
Ptr Addr is: 0xffffcc14.
```

# Accediendo al valor de un puntero

Se usa el **operador de indirección `*`**,

```c
    int number = 15;
    int *pointer = &number;
    int result = 0;
    result = *pointer + 5;

    printf("number address: %p, pointer address:%p.\n", &number, pointer);
```

Salida:

```ps
number address: 0xffffcc04, pointer address:0xffffcc04.
```

Tamaño que ocupa la variable del puntero que guarda la dirección a donde apunta:

```c
pointer = NULL;
printf("Pointer size in bytes: %d.\n", sizeof(pointer));
```

La salida depende del microprocesador del equipo, los equipos de 32 bits (4 bytes) pueden usar 2^32 direccionamientos (de 0 a 4294967295), mi equipo es de 64 bits por eso genera como salida:

```c
Pointer size in bytes: 8.
```

![](img/02.PNG)

<iframe height="600px" width="100%" src="https://repl.it/@IkerLandajuela/Pointers?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>
