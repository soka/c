#include <stdio.h>

int sum(int, int);
int prod(int, int);

int (*pFun)(int, int);

//int (*pFunArray[2])(int, int);

int (*pFunArray[2])(int, int) = {sum, prod};

// Ptr to func as func arg
int any_func(int (*pFun)(int, int), int x, int y);

typedef int (*callbackdef)(int, int);
int any_func_ex(callbackdef, int, int);

void main()
{
    int res;
    pFun = sum;
    res = pFun(2, 3);
    printf("Sum result is: %d\n", res);
    pFun = prod;
    res = pFun(3, 3);
    printf("Product result is: %d\n", res);

    //pFunArray[0] = sum;
    //pFunArray[1] = prod;
    printf("Array ptr: %d, %d\n", pFunArray[0](1, 1), pFunArray[1](2, 2));

    printf("any_func: %d.\n", any_func(sum, 2, 2));

    printf("any_func_ex: %d.\n", any_func_ex(sum, 3, 3));
}

int sum(int a, int b)
{
    return (a + b);
}

int prod(int a, int b)
{
    return (a * b);
}

int any_func(int (*pFun)(int, int), int x, int y)
{
    return pFun(x, y);
}

int any_func_ex(callbackdef pFun, int a, int b)
{
    return pFun(a, b);
}