[TOC]

# Declarando un puntero a una función

```c
int (*pFunction)(int);
```

# Llamando a una función usando el puntero

```c
#include <stdio.h>

int sum(int, int);
int prod(int, int);

int (*pFun)(int, int);

void main()
{
    int res;
    pFun = sum;
    res = pFun(2, 3);
    printf("Sum result is: %d\n", res);
    pFun = prod;
    res = pFun(3, 3);
    printf("Product result is: %d\n", res);
}

int sum(int a, int b)
{
    return (a + b);
}

int prod(int a, int b)
{
    return (a * b);
}
```

# Arreglo de punteros a funciones

Declaramos el array:

```c
int (*pFunArray[2])(int, int);
```

Inicializamos los punteros:

```c
pFunArray[0] = sum;
pFunArray[1] = prod;
```

Los dos pasos de arriba se pueden abreviar como `int (*pFunArray[2])(int, int) = {sum, prod};`.

Ejecución:

```c
printf("Array ptr: %d, %d\n", pFunArray[0](1, 1), pFunArray[1](2, 2));
```

# Punteros a funciones como argumentos

Las retrollamadas (_callbacks_) aportan flexibilidad a la aplicación, así podemos llamar a una función u otra dependiendo de la situación. Yo lo usaba mucho para pasar un puntero a una función y así simular un evento en un autómata ejecutándose en un _thread_ independiente que avisa al programa principal.

Declaración:

```c
int any_func(int (*pFun)(int, int), int x, int y);
printf("any_func: %d.\n", any_func(sum, 2, 2));
```

# Prototipo de callback

```c
typedef int (*callbackdef)(int, int);
int any_func_ex(callbackdef, int, int);
```

```c
int any_func_ex(callbackdef pFun, int a, int b)
{
    return pFun(a, b);
}
```

```c
printf("any_func_ex: %d.\n", any_func_ex(sum, 3, 3));
```

# Código de ejemplo

- GitLab: c / docs / funciones / puntero-a-funcion / [src](https://gitlab.com/soka/c/tree/master/docs/funciones/puntero-a-funcion/src)

<iframe height="400px" width="100%" src="https://repl.it/@IkerLandajuela/Pointers-to-Functions?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>

# Docker

```ps
PS> docker build -t function-pointer-app .
PS> docker run function-pointer-app ls /usr/src/function-pointer-app
PS> docker run -it --rm --name function-pointer-running-app function-pointer-app


PS> docker run --rm -v "$PWD":/usr/src/function-pointer-app -w /usr/src/function-pointer-app gcc:4.9 make
PS> docker run --rm -v "$PWD":/usr/src/function-pointer-app -w /usr/src/function-pointer-app gcc:4.9 gcc -o function-pointer-app function-pointer-app.c
```
