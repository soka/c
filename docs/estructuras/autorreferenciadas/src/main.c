#include <stdio.h>
#include <stdlib.h> // malloc
#include <assert.h>

// --------------- Declaración de datos ----------------------

/**
 * Elemento de la cola
 */
struct elt
{
    int val;
    struct elt *next;
};

/**
 * Cola FIFO
 */
struct queue
{
    struct elt *head;
    struct elt *tail;
};

//-----------------------------------------------------------------

/**
 * Comprueba si la cola está vacía
 */
int isQueueEmpty(const struct queue *q)
{
    return (q->head == NULL);
}

//-----------------------------------------------------------------

/**
 * Crear una cola vacía
 */
struct queue *createQueue(void)
{
    struct queue *p = NULL;
    p = malloc(sizeof(struct queue));
    p->head = p->tail = NULL;
    return p;
}

//----------------------------------------------

/**
 * Inserta un elemento en la cola
 */
void enq(struct queue *q, int value)
{
    // Creamos un nuevo elemento
    struct elt *e;
    e = malloc(sizeof(struct elt));
    assert(e);
    e->val = value;
    e->next = NULL;

    //
    if (q->head == NULL) // cola vacía, es el primer elemento
    {
        q->head = e;
    }
    else
    { // En caso contrario el último elemento apunta al nuevo
        q->tail->next = e;
    }

    // El nuevo elemento es el último
    q->tail = e;
}

//-----------------------------------------------------------------

/**
 * Extrae el primer elemento de la cola y retorna su valor
 */
int deq(struct queue *q)
{
    int ret;
    struct elt *e;

    assert(!isQueueEmpty(q));

    ret = q->head->val;

    e = q->head;
    q->head = q->head->next;

    free(e); // Liberamos la memoria que ocupaba

    return ret;
}

void printQueue(struct queue *q)
{
    struct elt *e;

    assert(!isQueueEmpty(q));

    for (e = q->head; e != NULL; e = e->next)
    {
        printf("%d ", e->val);
    }
    putchar('\n');
}

//-----------------------------------------------------------------

void destroyQueue(struct queue *q)
{
    while (!isQueueEmpty(q))
    {
        deq(q);
    }

    free(q);
}

//-----------------------------------------------------------------

int main(void)
{
    struct queue *q = NULL;

    q = createQueue();
    enq(q, 1);
    enq(q, 2);
    enq(q, 3);
    printQueue(q);
    deq(q);
    deq(q);
    printQueue(q);
    destroyQueue(q);

    return 0;
}