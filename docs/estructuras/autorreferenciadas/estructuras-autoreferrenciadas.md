[TOC]

# Cola FIFO con estructuras autorreferenciadas

## Declaración de las estructuras

### Elemento de la cola

Cada elemento define un valor de tipo entero y un puntero al siguiente elemento:

```c
struct elt
{
    int val;
    struct elt *next;
};
```

### Cola

La estructura que define la cola tiene dos punteros a elementos, uno apunta al primer elemento de la FIFO y el otro al último.

```c
struct queue
{
    struct elt *head;
    struct elt *tail;
};
```

## Funciones para operar con la cola

### Comprobar si la cola está vacía

Una función de comprobación básica que retorna 0 si la cola no tiene elementos, será de especial utilidad en el resto de las funciones y en el cuerpo principal del programa.

```c
int isQueueEmpty(const struct queue *q)
{
    return (q->head == NULL);
}
```

### Crear la cola

La siguiente función crea una cola vacía. Declaro un puntero a la estructura `queue` y reservo memoria, a continuación hago que los punteros del primer y último elemento apunten a `NULL` (puntero vacío). Al final de la función retorno el puntero:

```c
struct queue *createQueue(void)
{
    struct queue *p = NULL;
    p = malloc(sizeof(struct queue));
    p->head = p->tail = NULL;
    return p;
}
```

### Encolar un nuevo elemento

Lo primero es crear un nuevo elemento para albergar el valor que recibo como parámetro de entrada a la función. Si la cola está vacía será el primer elemento de la FIFO, en caso contrario lo posiciona al final de la cola, en cualquiera de los casos el puntero `tail` al último elemento queda apuntando a este nuevo elemento.

```c
void enq(struct queue *q, int value)
{
    struct elt *e;
    e = malloc(sizeof(struct elt));
    assert(e);
    e->val = value;
    e->next = NULL;

    if (q->head == NULL)
    {
        q->head = e;
    }
    else
    {
        q->tail->next = e;
    }

    q->tail = e;
}
```

### Deseconlar un elemento

En una cola FIFO en primer elemento en entrar es el primero en salir. Primero obtengo el valor del primer elemento, el puntero a un elemento hago que apunte al primer elemento sólo para poder liberar la memoria que tiene reservada.

```c
int deq(struct queue *q)
{
    int ret;
    struct elt *e;

    assert(!isQueueEmpty(q));

    ret = q->head->val;

    e = q->head;
    q->head = q->head->next;

    free(e); // Liberamos la memoria que ocupaba

    return ret;
}
```

### Imprimir cola por consola

Esta función apenas merece comentarla, lo más destacable es el bucle `for` que usa para recorrer la FIFO con un apuntador a elemento.

```c
void printQueue(struct queue *q)
{
    struct elt *e;

    assert(!isQueueEmpty(q));

    for (e = q->head; e != NULL; e = e->next)
    {
        printf("%d ", e->val);
    }
    putchar('\n');
}
```

### Destruir la cola

Lo único que hay que tener en cuenta es liberar la memoria reservada:

```c
void destroyQueue(struct queue *q)
{
    while (!isQueueEmpty(q))
    {
        deq(q);
    }

    free(q);
}
```

## Ejecución

```c
int main(void)
{
    struct queue *q = NULL;

    q = createQueue();
    enq(q, 1);
    enq(q, 2);
    enq(q, 3);
    printQueue(q);
    deq(q);
    deq(q);
    printQueue(q);
    destroyQueue(q);

    return 0;
}
```

## Código

- GitLab: c / docs / estructuras / autorreferenciadas / [src](https://gitlab.com/soka/c/tree/master/docs/estructuras/autorreferenciadas/src).

<iframe height="600px" width="100%" src="https://repl.it/@IkerLandajuela/struct-queue-fifo?lite=true" scrolling="no" frameborder="no" allowtransparency="true" allowfullscreen="true" sandbox="allow-forms allow-pointer-lock allow-popups allow-same-origin allow-scripts allow-modals"></iframe>
```
